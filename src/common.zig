// zig version 0.11.0-dev.3737+9eb008717
const expect = @import("std").testing.expect;

test "if statement" {
    const a = true;
    var x: u16 = 0;
    if (a) {
        x += 1;
    } else {
        x += 2;
    }
    try expect(x == 1);
}

test "if statement expression" {
    const a = true;
    var x: u16 = 0;
    x += if (a) 1 else 2;
    try expect(x == 1);
}

test "while" {
    var i: u8 = 2;
    while (i < 100) {
        i *= 2;
    }
    try expect(i == 128);
}

test "while with continue expression" {
    var sum: u8 = 0;
    var i: u8 = 1;
    while (i <= 10) : (i += 1) {
        sum += i;
    }
    try expect(sum == 55);
}

test "while with continue" {
    var sum: u8 = 0;
    var i: u8 = 0;
    while (i <= 3) : (i += 1) {
        if (i == 2) continue;
        sum += i;
    }
    try expect(sum == 4);
}

test "while with break" {
    var sum: u8 = 0;
    var i: u8 = 0;
    while (i <= 3) : (i += 1) {
        if (i == 2) break;
        sum += i;
    }
    try expect(sum == 1);
}

test "for" {
    // character literals are equivalent to integer literals
    const string = [_]u8{ 'a', 'b', 'c' };

    // enumeration
    for (string, 0..) |character, index| {
        _ = character; // zig does not allow us to have unused value
        _ = index;
    }

    for (string) |character| {
        _ = character;
    }

    for (string, 0..) |_, index| {
        _ = index;
    }

    for (string) |_| {}
}

// All function arguments are immutable
// If a copy is desired the user must explicitly make one
// Unlike variables which are snake_case, functions are camelCase.
fn addFive(x: u32) u32 {
    return x + 5;
}

test "function" {
    const y = addFive(0);
    try expect(@TypeOf(y) == u32);
    try expect(y == 5);
}

// fibonacci numbers : relationship between numbers
fn fibonacci(n: u16) u16 {
    if (n == 0 or n == 1) return n;
    return fibonacci(n - 1) + fibonacci(n - 2);
}

test "fibonacci recursion" {
    const x = fibonacci(10);
    try expect(x == 55);
}

// When recursion happens, the compiler is no longer able to work out the maximum stack size.
// This may result in unsafe behaviour - stack overflow

test "defer" {
    var x: i16 = 5;
    {
        defer x += 2;
        // used to excute a statement while exiting the current block.
        try expect(x == 5);
    }
    try expect(x == 7); // '==7' error??
}

test "multi defer" {
    var x: f32 = 5;
    {
        defer x += 2;
        defer x /= 2;
    }
    try expect(x == 4.5);
    // defers are executed in reverse order.
}

const FileOpenError = error{
    AccessDenied, // value..
    OutOfMemory,
    FileNotFound,
};
// An error set is like an enum, where each error in the set is a value.
// There are no exceptions in Zig; errors are values.

// Error sets coerce to their supersets
const AllocationError = error{OutOfMemory};

test "coerce error from a subset to a superset" {
    const err: FileOpenError = AllocationError.OutOfMemory; // coerce
    try expect(err == FileOpenError.OutOfMemory);
}

// An error set type and a narmal type can be combine with the ! operator to form an error union type.
// Values of these types may be an error value, or a value of the normal type.

// Let's create a value of an error union type.
// Here catch is used, which is followed by an expresion which is evaluated when the value before it is an error.
// The catch here is used to provide a fallback value, but could instead be a noreturn
// - the type of return, while (ture) and others.
test "error union" {
    const maybe_error: AllocationError!u16 = 10; // it can be an error or a value (it's u16 value at this time)
    const no_error = maybe_error catch 0; // use catch to fallback

    try expect(@TypeOf(no_error) == u16);
    try expect(no_error == 10);
}

// Functions often return error unions.
// Here's one using a catch, where the |err| syntax receives the value of the error.
// This is called payload capturing, and is usec similarly in many places.
// Side note: some languages use similar syntax for lambdas - this is not the case for Zig
fn failingFunction() error{Oops}!void {
    return error.Oops;
}

test "returning an error" {
    failingFunction() catch |err| {
        try expect(err == error.Oops);
        return;
    };
}

// try x is a shotcut for x catch |err| return err, and is commonly used in places where handling an error isn't appropriate.
// Zig's try and catch are unrelated to try-catch in other languages.
fn failFn() error{Oops}!i32 {
    try failingFunction(); // this will return an error
    return 12;
}

test "try" {
    var v = failFn() catch |err| {
        try expect(err == error.Oops);
        return;
    };
    try expect(v == 15); // is never reached
}
// errdefer works like defer, but only executing when the function is returned from with an error inside of the errdefer's block.
var problems: u32 = 98;

fn failFnCounter() error{Oops}!void {
    errdefer problems += 1; // only executing when the failFnCounter is returned with error
    try failingFunction();
}

test "errdefer" {
    failFnCounter() catch |err| {
        try expect(err == error.Oops);
        try expect(problems == 99);
        return;
    };
}

// Error unions returned from a fcuntion can have their error sets inferred by not having an explicit error set.
// This inferred error set contains all possible errors which the function may return.
fn createFile() !void {
    return error.AccessDenied;
}

test "inferred error set" {
    // type coercion successfully takes place (because this error set is a superset)
    const x: error{AccessDenied}!void = createFile();

    // Zig does not let us ignore error unions via _ = x;
    // we must unwrap it with "try" , "catch", or "if" by any means
    _ = x catch {};
}

// Error sets can be merged.
const A = error{ NotDir, PathNotFound };
const B = error{ OutOfMemory, PathNotFound };
const C = A || B;
// anyerror is the global error set which due to being the superset of all error sets,
// can have an error from any set coerce to a value of it.
// Its usage should be generally avoided.

// Zig's switch works as both a statement and an expression.
// The types of all branched must coerce to the type which is being switched upon. => ?
// All possible values must have an associated branch
// - values cannot be left out.
// Cases cannot fall through to other branches.

// An example of a switch statement. The else is required to satisfy the exhaustiveness of this switch.
test "switch statement" {
    var x: i8 = 10;
    switch (x) {
        -1...1 => {
            x = -x;
        },
        10, 100 => {
            // special considerations must be made
            // when dividing signed intergers
            x = @divExact(x, 10);
        },
        else => {},
    }
    try expect(x == 1);
}

// Here is the former, but as a switch expression.
test "switch expression" {
    var x: i8 = 10;
    x = switch (x) {
        -1...1 => -x,
        10, 100 => @divExact(x, 10),
        else => x,
    }; // expression
    try expect(x == 1);
}

// Values can be ignored by using _ in place of a variable of const declaration.
// This does not work at the global scope (only works inside functions and block)
// This is useful for ignoring the values returned from functions if you do not need them.
fn main() void {
    _ = 10;
}
