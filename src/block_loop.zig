const expect = @import("std").testing.expect;
// Labelled Blocks
// Blocks in Zig are expressions and can be given labels,
// which are used to yield values.
// Here, we are using a label called blk.
// Blocks yield values, meaning that they can be used in place of a value.
// The value of an empty block {} is a value of the type void.
test "labelled blocks" {
    // block is expression
    const count = blk: { // This can be labelled
        var sum: u32 = 0;
        var i: u32 = 0;
        while (i < 10) : (i += 1) sum += i;
        break :blk sum; // break and return
    };
    try expect(count == 45);
    try expect(@TypeOf(count) == u32);
}

// This can be seen as being equivalent to C's i++
// blk: {
//     const tmp = i;
//     i += 1;
//     break :blk tmp;
// }

// Labelled Loops
// Loops can be given labels, allowing you to break and countinue to outer loops.
test "nested continue" {
    var count: usize = 0;
    outer: for ([_]i32{ 1, 2, 3, 4, 5, 6, 7, 8 }) |_| {
        for ([_]i32{ 1, 2, 3, 4, 5 }) |_| {
            count += 1;
            continue :outer;
        }
    }
    try expect(count == 8);
}

// Loops as expressions
// Like return, break acceepts a value. This can be used to yield a value from a loop.
// Loops in Zig also have an else branch on loops,
// which is evaluated when the loop is not exited from with a break.
fn rangeHasNumber(begin: usize, end: usize, number: usize) bool {
    var i = begin;
    return while (i < end) : (i += 1) {
        if (i == number) {
            break true;
        }
    } else false;
}

test "while loop expression" {
    try expect(rangeHasNumber(0, 10, 3));
    // try expect(rangeHasNumber(0, 10, 11));
}
