const expect = @import("std").testing.expect;
// Structs
// Zig's most common kind of composite data type,
// allowing you to define types that can store a fixed set of named fields.
// Zig gives to guarantee about the in-memory order of fields in structs, or its size.
// Like arrays, structs are also neatly constructed with T{} syntax.

const Vec3 = struct { x: f32, y: f32, z: f32 };

test "struct usage" {
    const my_vector = Vec3{
        .x = 0,
        .y = 100,
        .z = 50,
    };
    _ = my_vector;
}

// All fields must be given a value. (Can't use empty structs)
// Fields may be given defaults.
const Vec4 = struct { x: f32, y: f32, z: f32, w: f32 = undefined };

test "struct default" {
    const my_vector = Vec4{
        .x = 25,
        .y = -50,
    };
    _ = my_vector;
}

// Like enums, structs may also contain functions and declarations.
// They have the unique property that when given a pointer to a struct,
// one level of dereferencing is done automatically when accessing fields.
// notice how in this example,
// self.x and self.y are accessed in the swap function without needing to dereference the self pointer

const Stuff = struct {
    x: i32,
    y: i32,
    // contained function
    fn swap(self: Stuff) void {
        const tmp = self.x; // auto dereferencing
        self.x = self.y;
        self.y = tmp;
    }
};

test "automatic dereference" {
    var thing = Stuff{ .x = 10, .y = 20 };
    thing.swap();
    try expect(thing.x == 20);
    try expect(thing.y == 10);
}
