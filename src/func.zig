const expect = @import("std").testing.expect;

fn addFive(x: u32) u32 { // all function arguments are immutable.
    return x + 5;
}

test "function" {
    const y = addFive(0); // Type of y inferred from function signature return type.
    try expect(@TypeOf(y) == u32);
    try expect(y == 5);
}

fn fibonacci(n: u16) u16 { // recursion => compiler can't work out the maximum stack size.
    if (n == 0 or n == 1) return n;
    return fibonacci(n - 1) + fibonacci(n - 2);
}

test "function recursion" {
    const x = fibonacci(10);
    try expect(x == 55);
}
