const expect = @import("std").testing.expect;

// C struct by extern
const Data = extern struct { a: i32, b: u8, c: f32, d: bool, e: bool };

test "hmn" {
    // use C struct in Zig
    const x = Data{
        .a = 10005,
        .b = 42,
        .c = -10.5,
        .d = false,
        .e = true,
    };
    const z = @as([*]const u8, @ptrCast(&x));

    try expect(@as(*const i32, @ptrCast(@alignCast(z))).* == 10005);
    try expect(@as(*const u8, @ptrCast(@alignCast(z + 4))).* == 42);
    try expect(@as(*const f32, @ptrCast(@alignCast(z + 8))).* == -10.5);
    try expect(@as(*const bool, @ptrCast(@alignCast(z + 12))).* == false);
    try expect(@as(*const bool, @ptrCast(@alignCast(z + 13))).* == true);
}

test "aligned pointers" {
    const a: u32 align(8) = 5;
    try expect(@TypeOf(&a) == *align(8) const u32);
}

pub fn main() void {
    // We can make specially aligned data by using the `align(x)` property.
    const a1: u8 align(8) = 100;
    _ = a1;

    const a2 align(8) = @as(u8, 100);
    _ = a2;

    // making data with a lesser alignment - isn't particularly useful
    const b1: u64 align(1) = 100;
    _ = b1;

    const b2 align(1) = @as(u64, 100);
    _ = b2;
}

fn total(a: *align(64) const [64]u8) u32 {
    var sum: u32 = 0;
    for (a) |elem| sum += elem;
    return sum;
}

test "passing aligned data" {
    // set 10 in the first 64 bytes ** 64
    const x align(64) = [_]u8{10} ** 64;
    try expect(total(&x) == 640);
}
