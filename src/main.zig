const std = @import("std");
const eql = std.mem.eql;
const ArrayList = std.ArrayList;
const test_allocator = std.testing.allocator;
const expect = std.testing.expect;

test "io writer usage" {
    var list = ArrayList(u8).init(test_allocator);
    defer list.deinit();
    const bytes_written = try list.writer().write( // ArrayList has a writer method
        "Hello World!",
    );
    try expect(bytes_written == 12);
    try expect(eql(u8, list.items, "Hello World!"));
}

test "io reader usage" {
    const message = "Hello File!";
    const file = try std.fs.cwd().createFile(
        "jink_file2.txt",
        .{ .read = true },
    );
    defer file.close();

    try file.writeAll(message);
    try file.seekTo(0); // go back to start of file

    const contents = try file.reader().readAllAlloc(
        test_allocator,
        message.len, // read by length of message
    );
    defer test_allocator.free(contents);
    try expect(eql(u8, contents, message));
}

// it returns error or null or string
fn nextLine(reader: anytype, buffer: []u8) !?[]const u8 {
    var line = (try reader.readUntilDelimiterOrEof(
        buffer,
        '\n', // set delimiter
    )) orelse return null; // if user did not type delimiter, return null

    // trim annoying windows-only carriage return character
    if (@import("builtin").os.tag == .windows) {
        return std.mem.trimRight(u8, line, "\r");
    } else {
        return line;
    }
}

test "read until next line" {
    const stdout = std.io.getStdOut();
    const stdin = std.io.getStdIn();

    try stdout.writeAll(
        \\ Enter your name:
    ); // print to stdout

    var buffer: [100]u8 = undefined;
    const input = (try nextLine(stdin.reader(), &buffer)).?; // read until next line
    try stdout.writer().print(
        "Your name is: \"{s}\"\n",
        .{input},
    );
}

// Don't create a type like this! Use an
// arraylist with a fixed buffer allocator
const MyByteList = struct {
    data: [100]u8 = undefined, // maximum size
    items: []u8 = &[_]u8{}, // current item location

    const Writer = std.io.Writer(
        *MyByteList, // context type
        error{EndofBuffer}, // error set
        appendWrite, // write function
    );

    fn appendWrite(
        self: *MyByteList,
        data: []const u8,
    ) error{EndofBuffer}!usize {
        if (self.items.len + data.len > self.data.len) {
            // check for overflow
            return error.EndofBuffer;
        }
        std.mem.copy(
            u8,
            self.data[self.items.len..], // slice data after previous write
            data, // overwrite
        );
        self.items = self.data[0 .. self.items.len + data.len];
        return data.len;
    }

    fn writer(self: *MyByteList) Writer {
        return .{ .context = self };
    }
};

test "custom writer" {
    var bytes = MyByteList{};
    const hello = try bytes.writer().write("Hello");
    const writer = try bytes.writer().write(" Writer!");
    try expect(hello == 5);
    try expect(writer == 8);
    try expect(eql(u8, bytes.items, "Hello Writer!"));
}

pub fn main() void {}
