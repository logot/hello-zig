// Opaque
// opaque types in Zig have an unknown (albeit non-zero) size and alignment.
// Because of this these data types cannot be stored directly.
// These are used to maintain types safety with pointers
// to types that we don't have information about.

// const Window = opaque {}; // unknown size - we don't have information about
// Opaque types may have declarations in their definitions
// (the same as strucsts, enums and unions).
const Window = opaque {
    fn show(self: *Window) void {
        show_window(self);
    }
};
const Button = opaque {};

extern fn show_window(*Window) callconv(.C) void;

test "opaque" {
    var main_window: *Window = undefined;
    show_window(main_window);

    var ok_button: *Button = undefined;
    show_window(ok_button);
}

test "opaque with declaratiosn" {
    var main_window: *Window = undefined;
    main_window.show();
}
// The typical usecase of opaque is to maintain type safety
// when interoperating with C code that does not expose complete type information
