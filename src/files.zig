const std = @import("std");
const expect = std.testing.expect;
const eql = std.mem.eql;

test "createFile, write, seekTo, read" {
    const file = try std.fs.cwd().createFile(
        "junk_file.txt",
        .{ .read = true }, // struct for options
    );
    defer file.close();

    const bytes_written = try file.writeAll("Hello File!"); // file usually can occur many error
    _ = bytes_written;

    var buffer: [100]u8 = undefined;
    try file.seekTo(0);
    const bytes_read = try file.readAll(&buffer);
    // save file content to &buffer
    // and return file buffers' bytes

    try expect(eql(u8, buffer[0..bytes_read], "Hello File!"));
}

test "file stat" {
    const file = try std.fs.cwd().createFile(
        "junk_file2.txt",
        .{ .read = true },
    );
    defer file.close();
    const stat = try file.stat();
    try expect(stat.size == 0);
    try expect(stat.kind == .file); // .File is deprecated
    try expect(stat.ctime <= std.time.nanoTimestamp());
    try expect(stat.mtime <= std.time.nanoTimestamp());
    try expect(stat.atime <= std.time.nanoTimestamp());
}

test "make dir" {
    try std.fs.cwd().makeDir("test-tmp");
    const iter_dir = try std.fs.cwd().openIterableDir(
        "test-tmp",
        .{},
    ); // Open dir
    defer {
        std.fs.cwd().deleteTree("test-tmp") catch unreachable;
    } // Close files in dir

    // Make files to dir
    _ = try iter_dir.dir.createFile("x", .{});
    _ = try iter_dir.dir.createFile("y", .{});
    _ = try iter_dir.dir.createFile("z", .{});

    var file_count: usize = 0;
    var iter = iter_dir.iterate(); // crate iterate
    while (try iter.next()) |entry| { // repeat until iterate isn't has next value
        if (entry.kind == .file) file_count += 1;
    }

    try expect(file_count == 3);
}
