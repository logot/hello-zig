const std = @import("std");

// Tagged Unions
const Item = union(enum) {
    foo,
    bar: []const u8,
    baz: usize,
};

const MyError = error{
    AccessDenied,
};

// fn createItem(num: usize) MyError!?Item{
// !? => What am i returning could be null
fn createItem(num: usize) !?Item {
    if (num == 420) {
        return MyError.AccessDenied;
    }

    if (num == 69) {
        return null;
    }
    return .foo;
}

pub fn main() !void {
    const item = try createItem(69); // throw Exception!!

    // |foo| => Only not null value in this. (optional)
    if (item) |foo| {
        std.log.info("foo: {}", .{foo});
    }

    const foo = item.?;
    std.log.info("foo: {?}", .{foo});
}
