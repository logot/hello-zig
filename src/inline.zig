const expect = @import("std").testing.expect;
// Inline loops
// inline loops are unrolled, and allow some things to happen
// which only work at compile time.
// Here we use a for, but a while works similarly.
test "inline for" {
    const types = [_]type{ i32, f32, u8, bool };
    var sum: usize = 0;
    inline for (types) |T| sum += @sizeOf(T);
    try expect(sum == 10);
}
// Using these for performance reasons is inadvisable unless you've tested that
// explicitly unrolling is faster;
// the compiler tends to make better decisions here than you.
