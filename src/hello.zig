const std = @import("std");

pub fn theprimeagent() void {
    std.debug.print("Hello, {s}!\n", .{"world"});

    // Assignment
    // const - indicates that identifier is a constant that stores an immutable value.
    const constant: i32 = 5; // signed 32-bit constant
    _ = constant; // zig does not allow us to have unused values.
    // var - indicates that identifier is a variable that stores a mutable value.
    var variable: u32 = 5000; // unsigned 32-bit variable
    _ = variable;
    // :type is a type annotation for identifier, and may be omitted if the data type of value can be inferred.
    // constants and variables must have a value.
    // const a: i32 = undefined;
    // _ = a;
    // var b: u32 = undefined;
    // _ = b;

    const a = [5]u8{ 'h', 'e', 'l', 'l', 'o' }; // can't shadow - Difference from Rust
    _ = a;
    const b = [_]u8{ 'w', 'o', 'r', 'l', 'd' };

    const length = b.len;
    _ = length;

    // @as performs an explicit type coercion
    const inferred_constant = @as(i32, 5);
    _ = inferred_constant;
    var inferred_variable = @as(u32, 5000);
    _ = inferred_variable;
}

pub fn main() void {
    std.debug.print("Hello, {s}!\n", .{"World"});
}
